import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Currency, CurrencyExchangeRate, ExchangePayload, DailyTimeSeries, WeeklyTimeSeries, MonthlyTimeSeries } from '../models/models';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(private httpClient: HttpClient) { }

  private openExchangeBasePath: string = "http://openexchangerates.org";
  private alphaVantageBasePath: string = "https://www.alphavantage.co/query";
  private apiKey: string = "5KKGGX7I9B8NT25V";

  getAllCurrencies(): Observable<Currency[]> {
    return this.httpClient.get<Currency[]>(this.openExchangeBasePath + "/api/currencies.json");
  }

  exchange(payload: ExchangePayload): Observable<CurrencyExchangeRate> {
    return this.httpClient.get<CurrencyExchangeRate>(this.alphaVantageBasePath + 
      "?function=CURRENCY_EXCHANGE_RATE&from_currency=" + payload.base + "&to_currency=" + payload.target + "&apikey=" + this.apiKey);
  }

  getDailysPlot(currency: string): Observable<DailyTimeSeries> {
    return this.httpClient.get<DailyTimeSeries> (this.alphaVantageBasePath + "?function=TIME_SERIES_DAILY&symbol=" + currency + "&apikey=" + this.apiKey);
  }

  getWeeklysPlot(currency: string): Observable<WeeklyTimeSeries> {
    return this.httpClient.get<WeeklyTimeSeries> (this.alphaVantageBasePath + "?function=TIME_SERIES_WEEKLY&symbol=" + currency + "&apikey=" + this.apiKey);
  }

  getMonthlysPlot(currency: string): Observable<MonthlyTimeSeries> {
    return this.httpClient.get<MonthlyTimeSeries> (this.alphaVantageBasePath + "?function=TIME_SERIES_MONTHLY&symbol=" + currency + "&apikey=" + this.apiKey);
  }
}
