import { Component, OnInit } from '@angular/core';
import { CurrencyService } from 'src/app/services/currency-service.service';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { ExchangePayload, Currency, plotMode, Record } from 'src/app/models/models';
import { tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private currencyService: CurrencyService, private fb: FormBuilder, private snackBar: MatSnackBar,) {

    this.loadingGlobal = true;
    this.currencyForm = fb.group({
      base: ['', Validators.required],
      target: ['', Validators.required]
    });

    this.plotForm = fb.group({
      mode: ['', Validators.required],
      currency: ['', Validators.required],
      date: [{ value: '', disabled: true }, Validators.required]
    });

    this.plotForm.valueChanges.subscribe((val: Object) => {

      if (val.hasOwnProperty("mode")) {
        if (this.tempMode !== val["mode"] && val["mode"]) {
          this.tempMode = val["mode"];
          this.onChangeForm();

          return;
        }
      }

      if (val.hasOwnProperty("currency")) {
        if (this.tempCurrency !== val["currency"] && val["currency"]) {
          this.tempCurrency = val["currency"];
          this.onChangeForm();

          return;
        }
      }
    });

    this.loadingGlobal = false;
  }

  private currencies: Currency[] = [];
  private rate: string = '';
  private loadingRate: boolean = false;
  private loadingPlot: boolean = false;
  private loadingGlobal: boolean = false;
  private validDates: Date[] = [];
  private currencyForm: FormGroup;
  private plotForm: FormGroup;
  private plotMode = plotMode;
  private tempMode = "";
  private tempCurrency = "";
  private currentTimeSeries = {};

  private googleChart = {
    title: '',
    type: 'LineChart',
    data: [],
    columnNames: ['Type', 'Value'],
    options: {
      width: 1000,
      height: 400,
    },
    show: false
  }

  ngOnInit() {

    this.currencyService.getAllCurrencies().subscribe(result => {

      Object.keys(result).forEach(key => {
        this.currencies.push({ code: key, description: result[key] });
      });

    }, err => {
      console.log(err);
    })
  }

  private onChangeForm() {

    let tempMode = this.plotForm.controls.mode.value;
    let tempCurrency = this.plotForm.controls.currency.value;

    if (tempMode && tempCurrency) {
      this.loadingGlobal = true;

      if (tempMode === plotMode.DAILY) {
        this.currencyService.getDailysPlot(tempCurrency).pipe(tap(res => {
          if (res["Time Series (Daily)"]) {
            this.currentTimeSeries = res["Time Series (Daily)"];
            this.parseDatesFromApi(res["Time Series (Daily)"]);
          } else {
            throw new Error("Invalid res");
          }
          this.loadingGlobal = false;
        })).subscribe(res => {

        }, err => {
          this.snackBar.open('Brak waluty', 'OK');
          console.log(err);
          this.resetForm();
          this.loadingGlobal = false;
        });
      } else if (tempMode === plotMode.WEEKLY) {
        this.currencyService.getWeeklysPlot(tempCurrency).pipe(tap(res => {
          if (res["Weekly Time Series"]) {
            this.currentTimeSeries = res["Weekly Time Series"];
            this.parseDatesFromApi(res["Weekly Time Series"]);
          } else {
            throw new Error("Invalid res");
          }
          this.loadingGlobal = false;
        })).subscribe(res => {

        }, err => {
          this.snackBar.open('Brak waluty', 'OK');
          console.log(err);
          this.resetForm();
          this.loadingGlobal = false;
        });
      } else {
        this.currencyService.getMonthlysPlot(tempCurrency).pipe(tap(res => {
          if (res["Monthly Time Series"]) {
            this.currentTimeSeries = res["Monthly Time Series"];
            this.parseDatesFromApi(res["Monthly Time Series"]);
          } else {
            throw new Error("Invalid res");
          }
          this.loadingGlobal = false;
        })).subscribe(res => {

        }, err => {
          this.snackBar.open('Brak waluty', 'OK');
          console.log(err);
          this.resetForm();
          this.loadingGlobal = false;
        })
      }
    }
  }

  resetForm() {
    this.plotForm.controls.date.disable();
    this.plotForm.controls.date.reset();
    this.googleChart.show = false;
  }

  myFilter = (d: Date): boolean => {

    return (this.validDates.find(v => v.getTime() === d.getTime())) !== undefined ? true : false;
  }

  private parseDatesFromApi(dates: { [key: string]: Record }) {
    this.validDates = [];
    let lastVal: Date;
    let i = 0;
    Object.keys(dates).forEach(val => {
      let d: Date = new Date(val);
      d.setHours(0);
      d.setMinutes(0);
      d.setSeconds(0);
      d.setMilliseconds(0);
      if (i === 0) {
        lastVal = d;
        ++i;
      }
      this.validDates.push(d);
    });

    this.plotForm.controls.date.setValue(lastVal);
    this.plotForm.controls.date.enable();
  }

  private generatePlot() {

    this.googleChart.show = false;
    this.loadingPlot = true;

    let date: Date = this.plotForm.controls.date.value;
    let key: string = date.getFullYear() + "-" + this.addPrefixZero(date.getMonth() + 1) + "-" + this.addPrefixZero(date.getDate());
    let timeSeries = this.currentTimeSeries[key];
    this.googleChart.data = [];
    Object.keys(timeSeries).forEach(val => {
      let splitted: string[] = val.split(' ');
      if (splitted[1] !== 'volume') {
        this.googleChart.data.push([splitted[1], Number.parseFloat(timeSeries[val])]);
      }
    });

    this.loadingPlot = false;
    this.googleChart.show = true;
    this.plotForm.enable();
  }

  private addPrefixZero(num: number): string {
    if (num < 10) return "0" + num;
    else return num.toString();
  }

  private transfer(formDirective: FormGroupDirective) {

    this.loadingRate = true;
    const payload: ExchangePayload = {
      base: this.currencyForm.controls.base.value,
      target: this.currencyForm.controls.target.value
    }

    this.currencyForm.reset();
    formDirective.resetForm();
    this.currencyService.exchange(payload).subscribe(response => {
      this.rate = payload.base + " to " + payload.target + " = " + response["Realtime Currency Exchange Rate"]["5. Exchange Rate"];
      this.loadingRate = false;
    }, err => {
      console.log(err);
    });
  }

}
