export interface Currency {
    code: string;
    description: string;
}

export interface CurrencyExchangeRate {
    "Realtime Currency Exchange Rate": {
        "1. From_Currency Code": string,
        "2. From_Currency Name": string,
        "3. To_Currency Code": string,
        "4. To_Currency Name": string,
        "5. Exchange Rate": string,
        "6. Last Refreshed": string,
        "7. Time Zone": string,
        "8. Bid Price": string,
        "9. Ask Price": string
    }
}

export interface ExchangePayload {
    base: string,
    target: string
}

export enum plotMode {
    DAILY = 'daily',
    WEEKLY = 'weekly',
    MONTHLY = 'monthly'
}

export interface Record {
    "1. open": string,
    "2. high": string,
    "3. low": string,
    "4. close": string,
    "5. volume": string
}

export interface DailyTimeSeries {
    "Meta Data": {
        "1. Information": string,
        "2. Symbol": string,
        "3. Last Refreshed": Date,
        "4. Output Size": string,
        "5. Time Zone": string
    },
    "Time Series (Daily)": {
        [key: string]: Record
    }
}

export interface WeeklyTimeSeries {
    "Meta Data": {
        "1. Information": string,
        "2. Symbol": string,
        "3. Last Refreshed": Date,
        "4. Time Zone": string
    },
    "Weekly Time Series": {
        [key: string]: Record
    }
}

export interface MonthlyTimeSeries {
    "Meta Data": {
        "1. Information": string,
        "2. Symbol": string,
        "3. Last Refreshed": Date,
        "4. Time Zone": string
    },
    "Monthly Time Series": {
        [key: string]: Record
    }
}